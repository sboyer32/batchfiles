@echo off

chdir "C:\Users\sean.boyer\Documents\PlanningShortcuts"

start "" "_Patient Tables.lnk"
start "" "Cygwin.lnk"
start "" "DVHReport_v3.1.7.lnk"
start "" "focus_output.lnk"
start "" "Initial Chart Check (SL,HR).lnk"
start "" "Plan Checking.lnk"
start "" "ProCureMU.lnk"
start "" "_Initial Check Template.lnk"

rem Only need to use the below if you are troubleshooting (keeps window from going away)
rem timeout /t -1