@echo off

chdir "C:\Users\sean.boyer\Documents\PlanningShortcuts"

start "" "Initial Chart Check (SL,HR).lnk"
start "" "Plan Checking.lnk"
start "" "Robustness and Constrainst Analysis.lnk"
start "" "SPC Excerpts and How To.lnk"
start "" "_Initial Check Template.lnk"

rem Only need to use the below if you are troubleshooting (keeps window from going away)
rem timeout /t -1