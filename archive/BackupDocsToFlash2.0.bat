@echo off 
REM Don't display commands unless the command is echo

REM Backing up My Docs, Cygwin and XiO scripts

REM Code from https://stackoverflow.com/questions/605522/print-time-in-a-batch-file-milliseconds
setlocal

rem The format of %TIME% is HH:MM:SS,CS for example 23:59:59,99
set STARTTIME=%TIME%

rem ********* Here begins the command you want to measure *************
xcopy /s/y C:\Users\sean.boyer\Documents E:\BackupWordDocs
xcopy /s/y C:\cygwin64\home\sean.boyer E:\BackupCygwin
call BackupXiOFiles.bat
rem ********* Here ends the command you want to measure ***************

set ENDTIME=%TIME%

rem output as time
echo STARTTIME: %STARTTIME%
echo ENDTIME: %ENDTIME%

rem convert STARTTIME and ENDTIME to centiseconds
set /A STARTTIME=(1%STARTTIME:~0,2%-100)*360000 + (1%STARTTIME:~3,2%-100)*6000 + (1%STARTTIME:~6,2%-100)*100 + (1%STARTTIME:~9,2%-100)
set /A ENDTIME=(1%ENDTIME:~0,2%-100)*360000 + (1%ENDTIME:~3,2%-100)*6000 + (1%ENDTIME:~6,2%-100)*100 + (1%ENDTIME:~9,2%-100)

rem calculating the duratyion is easy
set /A DURATION=(%ENDTIME%-%STARTTIME%) * 10

rem we might have measured the time inbetween days
if %ENDTIME% LSS %STARTTIME% set set /A DURATION=%STARTTIME%-%ENDTIME%

rem now break the centiseconds down to hors, minutes, seconds and the remaining centiseconds
set /A DURATIONH=%DURATION% / 360000
set /A DURATIONM=(%DURATION% - %DURATIONH%*360000) / 6000
set /A DURATIONS=(%DURATION% - %DURATIONH%*360000 - %DURATIONM%*6000) / 100
set /A DURATIONHS=(%DURATION% - %DURATIONH%*360000 - %DURATIONM%*6000 - %DURATIONS%*100)

rem some formatting
if %DURATIONH% LSS 10 set DURATIONH=0%DURATIONH%
if %DURATIONM% LSS 10 set DURATIONM=0%DURATIONM%
if %DURATIONS% LSS 10 set DURATIONS=0%DURATIONS%
if %DURATIONHS% LSS 10 set DURATIONHS=0%DURATIONHS%

rem outputing
REM echo STARTTIME: %STARTTIME% centiseconds
REM echo ENDTIME: %ENDTIME% centiseconds
echo DURATION: %DURATION% in ms
REM echo %DURATIONH%:%DURATIONM%:%DURATIONS%.%DURATIONHS%

REM # Let the user see the info before closing the window
timeout /t -1

endlocal
goto :EOF

