set YYYY=%DATE:~10,4%
set MM=%DATE:~4,2%
set DD=%DATE:~7,2%

set HH=%TIME: =0%
set HH=%HH:~0,2%
set MI=%TIME:~3,2%
set SS=%TIME:~6,2%
set FF=%TIME:~9,2%

set fileName=backupLog%YYYY%-%MM%-%DD%_%HH%%MI%%SS%.txt

>>log.txt (
echo %fileName%
)
