@echo off 
REM Don't display commands unless the command is echo

REM # Start the timer, and copy My Docs to the flash drive
set STARTTIME=%TIME%
xcopy /s/y C:\Users\sean.boyer\Documents E:\WorkDocsBackup
set ENDTIME=%TIME%

REM # Display the start and end times
echo Start: %STARTTIME%
echo End:   %ENDTIME%

REM # Let the user see the info before closing the window
timeout /t -1